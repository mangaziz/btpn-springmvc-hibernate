package com.angaziz.test;

import com.angaziz.test.domain.entity.User;
import com.angaziz.test.domain.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestBill {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testWhenEmployee() {
        User user = userRepository.findOneByIsEmployee(true);
        double totalBill = (new Bill(user, false, 200)).calculate();

        /**
         * How is this work?
         * 200 - employee discount = 140 => employee discount: 30%
         * 140 which is multiply 1 in 100, means: 1 * 5 = 5
         * 140 - 5 = $ 135
         */

        assertEquals(135, (int) totalBill);
    }

    @Test
    public void testWhenAffiliate() {
        User user = userRepository.findOneByIsAffiliate(true);
        double totalBill = (new Bill(user, false, 200)).calculate();

        /**
         * How is this work?
         * 200 - affiliate discount = 170 => employee discount: 15%
         * 170 which is multiply 1 in 100, means: 1 * 5 = 5
         * 170 - 5 = $ 165
         */

        assertEquals(165, (int) totalBill);
    }
}
