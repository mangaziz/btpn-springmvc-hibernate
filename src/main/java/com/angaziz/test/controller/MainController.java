package com.angaziz.test.controller;

import com.angaziz.test.Bill;
import com.angaziz.test.domain.entity.User;
import com.angaziz.test.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@ComponentScan
public class MainController {
    // Injecting repositories
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(name = "/", method = RequestMethod.GET)
    public String formShow(Model model) {
        // Preparing data to pass
        model.addAttribute("customers", userRepository.findAll());

        return "index";
    }

    @RequestMapping(name = "/", method = RequestMethod.POST)
    public String formProcess(@RequestParam("customer") int userID,
                              @RequestParam(name = "grocery", required = false) boolean grocery,
                              @RequestParam("bill") double bill,
                              Model model) {
        // Getting user information and instantiate bill calculator
        User user = userRepository.findOne(userID);
        double totalBill = (new Bill(user, grocery, bill)).calculate();

        // Adding total bill to model
        model.addAttribute("name", user.getFullname());
        model.addAttribute("bill", bill);
        model.addAttribute("difference", (bill - totalBill));
        model.addAttribute("total", totalBill);

        return "result";
    }
}
