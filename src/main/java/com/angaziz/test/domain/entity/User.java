package com.angaziz.test.domain.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue
    private int id;
    @NotNull
    private String fullname;
    @NotNull
    @Column(name = "is_employee", columnDefinition = "false")
    private boolean isEmployee;
    @NotNull
    @Column(name = "is_affiliate", columnDefinition = "false")
    private boolean isAffiliate;
    @NotNull
    @Column(name = "joined_at")
    private Date joinedAt;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isEmployee() {
        return isEmployee;
    }

    public void setEmployee(boolean employee) {
        isEmployee = employee;
    }

    public boolean isAffiliate() {
        return isAffiliate;
    }

    public void setAffiliate(boolean affiliate) {
        isAffiliate = affiliate;
    }

    public Date getJoinedAt() {
        return joinedAt;
    }

    public void setJoinedAt(Date joinedAt) {
        this.joinedAt = joinedAt;
    }

    @PrePersist
    void joinedAt() {
        this.setJoinedAt(new Date());
    }
}
