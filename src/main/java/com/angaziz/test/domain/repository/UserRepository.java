package com.angaziz.test.domain.repository;

import com.angaziz.test.domain.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findOneByIsEmployee(boolean isEmployee);
    User findOneByIsAffiliate(boolean isAffiliate);
}
