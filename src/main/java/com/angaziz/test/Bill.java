package com.angaziz.test;

import com.angaziz.test.domain.entity.User;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Bill {
    // Prequisites
    private User user;
    private boolean isGrocery;
    private double bill;

    // Result
    private double totalBill = 0;

    public Bill(User user, boolean isGrocery, double bill) {
        this.user = user;
        this.isGrocery = isGrocery;
        this.bill = bill;
    }

    public double calculate() {
        // Apply discount only when it's not a grocery
        double discount = 0;
        if(! isGrocery) {
            if(user.isEmployee()) {
                discount = 0.3;
            } else if(user.isAffiliate()) {
                discount = 0.15;
            } else {
                // Comparing user's join date with current date
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
                int nowYear = Integer.parseInt(simpleDateFormat.format(new Date()));
                int userYear = Integer.parseInt(simpleDateFormat.format(user.getJoinedAt()));
                int diffYear = nowYear - userYear;

                // Only if more than two year
                if(diffYear > 2) {
                    discount = 0.05;
                }
            }
        }

        // Calculating discount
        totalBill = bill - ( bill * discount );

        // Give $5 discount every $100
        if(totalBill > 100) {
            // Multiply and round down
            int multiply  = (int) Math.floor(totalBill / 100);
            totalBill = totalBill - (multiply * 5);
        }

        return totalBill;
    }
}
