CREATE TABLE `users` (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fullname VARCHAR(255) DEFAULT NULL,
  is_employee TINYINT(1) NOT NULL DEFAULT 0,
  is_affiliate BIGINT(20) NOT NULL DEFAULT 0,
  joined_at DATETIME NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

INSERT INTO `users` (`fullname`, `is_employee`, `is_affiliate`, `joined_at`) VALUES
  ('John Ordinary', false, false, DATE(NOW())),
  ('Stefan Employee', true, false, DATE(NOW())),
  ('Michael Affiliate', false, true, DATE(NOW())),
  ('Federighi Three Year', true, true, DATE(NOW() - INTERVAL 3 YEAR));